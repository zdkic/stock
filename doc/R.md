**1. 使用R語言的quantmod來抓取股票資訊**
 - 參考資料
   - [Data Science 到底是什麼從一個完全外行角度來看 用R來看股票，透過quantmod了解R的強大](https://blog.alantsai.net/posts/2018/01/data-science-series-16-r-hello-world-with-stock-analysis-using-quantmod)

   - [如何使用R 的 Quantmod 套件快速蒐集股價資訊並計算技術指標?](https://www.youtube.com/watch?v=BMhk2W2CTI8&t=79s)
   
   - [R語言101](https://yijutseng.github.io/DataScienceRBook/index.html)
   
   - [R - Data Frames](https://www.tutorialspoint.com/r/r_data_frames)
   

 - 安裝quantmod套件
 
   - 指令安裝
     ```R
     if (!require(quantmod))  install.packages("quantmod")
     ```

    - RStudio安裝
 
 ![](img/quantmod_install.png "")
 
 - 程式範例
   - 抓取兆豐金(2886)的股票資訊
 ```R
 library(quantmod) # 匯入quantmod 套件
 # 抓取兆豐金(2886.TW)的股票資訊
 tw2886 = getSymbols("2886.TW", from="2018-1-1", auto.assign = FALSE)
 chartSeries(tw2886,name="2886.TW",up.col='red',dn.col='green')  # 繪製K線圖
 ma5<-runMean(tw2886[,4],n=5)
 addTA(ma5,on=1,col="blue") # 五日均線
 ma20<-runMean(tw2886[,4],n=20)
 addTA(ma20,on=1,col="red") # 二十日均線
 addBBands() # 布林通道
 ```
 
   - 產生K線圖
 ![](img/quantmod_graph.png "")

   - 提示使用者輸入股市代號並產生K線圖
   ```R
    stockview<- function(id){
        stock <- getSymbols(paste(id,'.TW',sep=''),from = "2019-01-01",to = Sys.Date(),src = "yahoo",auto.assign = FALSE)
        View(stock)
        chartSeries(stock,theme='white',name=paste(x),up.col='red',dn.col='green')
        return(stock)
    }
    
    x <- readline(prompt="Enter stock: ")
    
    stock <- stockview(x)
    
    ma5 <- runMean(stock[,4],n=5)
    addTA(ma5,on=1,col="blue") # 五日均線
    
    ma20 <- runMean(stock[,4],n=20)
    addTA(ma20,on=1,col="red") # 二十日均線
   ```
   
   - 統計分析(最大值/最小值/平均值)
   ```R
   > summary(stock)
     Index             2886.TW.Open    2886.TW.High    2886.TW.Low    2886.TW.Close   2886.TW.Volume    
     Min.   :2019-01-02   Min.   :25.25   Min.   :25.40   Min.   :25.20   Min.   :25.25   Min.   : 5922810  
     1st Qu.:2019-03-05   1st Qu.:27.07   1st Qu.:27.12   1st Qu.:26.98   1st Qu.:27.10   1st Qu.:12766409  
     Median :2019-04-26   Median :28.75   Median :28.95   Median :28.70   Median :28.95   Median :18348874  
     Mean   :2019-04-24   Mean   :29.02   Mean   :29.17   Mean   :28.89   Mean   :29.06   Mean   :19290861  
     3rd Qu.:2019-06-18   3rd Qu.:30.95   3rd Qu.:31.10   3rd Qu.:30.80   3rd Qu.:30.93   3rd Qu.:24529098  
     Max.   :2019-08-07   Max.   :32.30   Max.   :32.50   Max.   :32.20   Max.   :32.30   Max.   :54441826  
     2886.TW.Adjusted
     Min.   :25.25   
     1st Qu.:27.10   
     Median :28.95   
     Mean   :29.06   
     3rd Qu.:30.93   
     Max.   :32.30
   ```

   - 繪製多檔股票的k線圖
  ```R
     for (idx in seq(from=2880, to=2887)) {
      id = paste(idx,".TW",sep="")
      data = getSymbols(id, from="2018-1-1", auto.assign = FALSE)
      chartSeries(data,name=id)
    }
  ```

   - 加權指數

   ![](img/twii.png "")
   ```R
     twii=getSymbols("^TWII", from="2019-1-1",auto.assign = FALSE)
     chartSeries(twii, name="加權指數")
   ```

   - 轉換為data frame

     ```R
     df = as.data.frame(tw2886)
     #印出成交量
     df[,"2886.TW.Volume"]
     ```

   - 列出滿足條件的股票
   ```R
    #計算兆豐金收盤價的變化率
    change_rate=Delt(Cl(tw2886))
    #列出收盤價變化率小於-2%的日期
    change_rate[which(change_rate < -0.02)]
   ```
   

**2. 使用網路爬蟲抓取股票三大法人買賣超**
 - 參考資料
   - [使用 R 與 rvest 套件擷取網頁資料](https://blog.gtwang.org/r/rvest-web-scraping-with-r/)

 - 安裝rvest
   ```R
    if (!require(rvest)) install.packages("rvest")
    library(rvest)
   ```

 - 抓取兆豐金(2886)的三大法人資訊
   - 使用google chrome的開發者工具找到三大法人的xpath

  ![](img/get_xpath.png "")

  ```R
    library(rvest)
    #抓取兆豐金(2886)的三大法人
    source <- read_html("https://www.wantgoo.com/stock/astock/three?stockno=2886")
    # 透過xpath抓取三大法人的資料 
    tbody <- html_nodes(source, xpath ="//*[@id=\"container\"]/div[7]/div[2]/table/tbody") 
    # 取得所有天數的三大法人 
    all_days <- tbody %>% html_nodes("tr")
    # 抓取標題
    tdate <- html_nodes(source, xpath ="//*[@id=\"container\"]/div[7]/div[2]/table/thead/tr[1]/th[1]")
    thead <- html_nodes(source, xpath ="//*[@id=\"container\"]/div[7]/div[2]/table/thead/tr[2]")
    heads <- thead %>% html_nodes("th")
    all_heads <- c()
    all_heads <- c(all_heads, html_text(tdate))
    for(idx in c(1:length(heads))) {
      all_heads <- c(all_heads, html_text(heads[idx]))
    }
    # 建立三大法人data frame
    date=a1=a2=a3=a4=a5=a6=a7=a8=a9=a10=a11=a12=c()
    df <- data.frame(date,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12)
    # 更新內容
    for(idx in c(1:(length(all_days)-2))){
        columes <- all_days[idx] %>% html_nodes("td") %>% html_text
        new_row <- data.frame(date=columes[1], 
                             a1=columes[2],
                             a2=columes[3],
                             a3=columes[4],
                             a4=columes[5],
                             a5=columes[6],
                             a6=columes[7],
                             a7=columes[8],
                             a8=columes[9],
                             a9=columes[10],
                             a10=columes[11],
                             a11=columes[12],
                             a12=columes[13])
        df <- rbind(df, new_row)
    }
    # 更新標題
    for(idx in c(1:length(all_heads))) {
      names(df)[idx] <- all_heads[idx]
    }

  ```

  ![](img/get_data.png "")
  
  
   - 列出當日外資買超前50名
   ```R
library(rvest)
# 抓取外資及陸資買賣超前50名的網頁內容
source <- read_html("https://www.wantgoo.com/stock/twstock/threebuysellrank?type=%E5%A4%96%E8%B3%87")
# 透過xpath抓取買超排名的資料
tbody<- html_nodes(source, xpath ="//*[@id=\"buyList\"]/tbody")
# 抓取排名前50名的資料
foreign_buy=tbody %>% html_nodes("tr")
# 印出內容
for(idx in c(1:length(foreign_buy))) {
    columes = foreign_buy[idx] %>% html_nodes("td")
    #抓取股票代號
    id <- columes %>% html_nodes("a") %>% html_attr("href")
    id <- strsplit(id, "/")[[1]][3]
    #印出資料 ex: 1 富邦VIX          ( 00677U ) 47,871
    cat(html_text(columes[1]), html_text(columes[2]), "(", id, ")", html_text(columes[3]), "\n")
}
   ```
   
   ![](img/foreign_buy.png "")