```bash
$ python finance.py

rate = 0.08
add_money_per_year = 450000
add_money_years = 10
add_extra_money_array = [0, 0, 0, 0, 0, 0, 700000, 400000]
use_money = 600000
use_money_from_year = 12
total_years = 30
Year  1  add money = 450000  add extra money = 0  use money = 0  principal = 486000(+36000 +8%)  interest = 36000
Year  2  add money = 450000  add extra money = 0  use money = 0  principal = 1010880(+110880 +12%)  interest = 74880
Year  3  add money = 450000  add extra money = 0  use money = 0  principal = 1577750(+227750 +16%)  interest = 116870
Year  4  add money = 450000  add extra money = 0  use money = 0  principal = 2189970(+389970 +21%)  interest = 162220
Year  5  add money = 450000  add extra money = 0  use money = 0  principal = 2851168(+601168 +26%)  interest = 211197
Year  6  add money = 450000  add extra money = 0  use money = 0  principal = 3565261(+865261 +32%)  interest = 264093
Year  7  add money = 450000  add extra money = 700000  use money = 0  principal = 5092482(+1242482 +32%)  interest = 377220
Year  8  add money = 450000  add extra money = 400000  use money = 0  principal = 6417881(+1717881 +36%)  interest = 475398
Year  9  add money = 450000  add extra money = 0  use money = 0  principal = 7417311(+2267311 +44%)  interest = 549430
Year 10  add money = 450000  add extra money = 0  use money = 0  principal = 8496696(+2896696 +51%)  interest = 629384
Year 11  add money = 0  add extra money = 0  use money = 0  principal = 9176432(+3576432 +63%)  interest = 679735
Year 12  add money = 0  add extra money = 0  use money = 600000  principal = 9910546(+4310546 +76%)  interest = 734114
Year 13  add money = 0  add extra money = 0  use money = 600000  principal = 10703390(+5103390 +91%)  interest = 792843
Year 14  add money = 0  add extra money = 0  use money = 600000  principal = 10911661(+5311661 +94%)  interest = 808271
Year 15  add money = 0  add extra money = 0  use money = 600000  principal = 11136594(+5536594 +98%)  interest = 824932
Year 16  add money = 0  add extra money = 0  use money = 600000  principal = 11379522(+5779522 +103%)  interest = 842927
Year 17  add money = 0  add extra money = 0  use money = 600000  principal = 11641883(+6041883 +107%)  interest = 862361
Year 18  add money = 0  add extra money = 0  use money = 600000  principal = 11925234(+6325234 +112%)  interest = 883350
Year 19  add money = 0  add extra money = 0  use money = 600000  principal = 12231253(+6631253 +118%)  interest = 906018
Year 20  add money = 0  add extra money = 0  use money = 600000  principal = 12561753(+6961753 +124%)  interest = 930500
Year 21  add money = 0  add extra money = 0  use money = 600000  principal = 12918694(+7318694 +130%)  interest = 956940
Year 22  add money = 0  add extra money = 0  use money = 600000  principal = 13304189(+7704189 +137%)  interest = 985495
Year 23  add money = 0  add extra money = 0  use money = 600000  principal = 13720524(+8120524 +145%)  interest = 1016335
Year 24  add money = 0  add extra money = 0  use money = 600000  principal = 14170166(+8570166 +153%)  interest = 1049641
Year 25  add money = 0  add extra money = 0  use money = 600000  principal = 14655780(+9055780 +161%)  interest = 1085613
Year 26  add money = 0  add extra money = 0  use money = 600000  principal = 15180242(+9580242 +171%)  interest = 1124462
Year 27  add money = 0  add extra money = 0  use money = 600000  principal = 15746661(+10146661 +181%)  interest = 1166419
Year 28  add money = 0  add extra money = 0  use money = 600000  principal = 16358394(+10758394 +192%)  interest = 1211732
Year 29  add money = 0  add extra money = 0  use money = 600000  principal = 17019066(+11419066 +203%)  interest = 1260671
Year 30  add money = 0  add extra money = 0  use money = 600000  principal = 17732591(+12132591 +216%)  interest = 1313525

```