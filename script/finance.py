import sys
import os
import math

def print_args(**args):
    print("rate = %s"%args['rate'])
    print("add_money_per_year = %s"%args['add_money_per_year'])
    print("add_money_years = %s"%args['add_money_years'])
    print("add_extra_money_array = %s"%args['add_extra_money_array'])
    print("use_money = %s"%args['use_money'])
    print("use_money_from_year = %s"%args['use_money_from_year'])
    print("total_years = %s"%args['total_years'])

def calc_stock_income(**args):
    print_args(**args)
    principal = 0
    interest = 0
    idx = 0
    base_money = 0
    extra_money_list = args['add_extra_money_array']
    for idx in range(0, int(args['add_money_years'])):
        principal += int(args['add_money_per_year'])
        base_money += int(args['add_money_per_year'])
        if idx < len(extra_money_list):
            principal += extra_money_list[idx]
            base_money += extra_money_list[idx]
        if idx >= int(args['use_money_from_year']):
            principal -= int(args['use_money'])
        interest = principal * float(args['rate']) 
        principal += interest
        if principal < 0:
            print("No Money!")
            sys.exit()
        extra_money = 0
        if idx < len(extra_money_list):
            extra_money = extra_money_list[idx]
        use_money = 0
        if idx >= int(args['use_money_from_year']) - 1:
            use_money = int(args['use_money'])
        print("Year %2d  add money = %d  add extra money = %d  use money = %d  principal = %d(+%d +%d%%)  interest = %d"%(
              idx+1, 
              int(args['add_money_per_year']), 
              extra_money,
              use_money,
              principal,
              principal - base_money,
              math.floor((principal - base_money)*100/base_money),
              interest))
    prev_idx = idx
    for idx in range(prev_idx+1, args['total_years']):
        if idx < len(extra_money_list):
            principal += extra_money_list[idx]
            base_money += extra_money_list[idx]
        if idx > int(args['use_money_from_year']):
            principal -= int(args['use_money'])
        interest = principal * float(args['rate']) 
        principal += interest
        if principal < 0:
            print("No Money!")
            sys.exit()
        extra_money = 0
        if idx < len(extra_money_list):
            extra_money = extra_money_list[idx]
        use_money = 0
        if idx >= int(args['use_money_from_year']) - 1:
            use_money = int(args['use_money'])
        print("Year %2d  add money = %d  add extra money = %d  use money = %d  principal = %d(+%d +%d%%)  interest = %d"%(
              idx+1, 
              0, 
              extra_money,
              use_money,
              principal,
              principal - base_money,
              math.floor((principal - base_money)*100/base_money),
              interest))


if __name__ == '__main__':
   calc_stock_income(rate=0.08,
                     add_money_per_year=45 * 10000,
                     add_money_years=10,
                     add_extra_money_array=[0,0,0,0,0,60*10000],
                     use_money=60*10000,
                     use_money_from_year=12,
                     total_years=30)